<?php
//不允许单独访问本文件
if(!defined('IN_JNSQTA')){
    exit(ACCESS_DENIED);
}


/**
 * 通过Xml配置文件实现拦截检查
 */
class XmlInterceptor implements Interceptor {
    private $filename = NULL;
    private $xmlObject = NULL;

    /**
     * XmlInterceptor constructor.
     * @param $filename 文件地址
     */
    public function __construct($filename)
    {
        libxml_disable_entity_loader(true);

        $this->filename = $filename;
        $this->xmlObject = simplexml_load_string(file_get_contents($this->filename));

        if(!$this->xmlObject){
            Log::write($this->filename . ' 不存在或加载失败...');
        }
    }

    /**
     * 返回所有需要login拦截的方法名，获取不成功返回空数组
     *
     * @return array()
     */
    public function getLoginCheck()
    {
        return $this->getActions('login_check','action');
    }

    /**
     * 返回所有需要token拦截的方法名，获取不成功返回空数组
     *
     * @return array()
     */
    public function getSignCheck(){
        return $this->getActions('sign_check','action');
    }

    private function getActions($childName , $nodeName){
        $values = array();

        if($this->xmlObject->$childName){
            $xmlContent = get_object_vars($this->xmlObject->$childName);

            if(!is_array($xmlContent[$nodeName])){//如果只有一个节点，就封装为数组
                $values[] = $xmlContent[$nodeName];
            }
            else{
                $values = $xmlContent[$nodeName];
            }
        }

        return $values;
    }

    public function __destruct()
    {
        unset($this->xmlObject);
    }


}