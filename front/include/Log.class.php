<?php
//不允许单独访问本文件
if(!defined('IN_JNSQTA')){
    exit(ACCESS_DENIED);
}

/**
 * 日志处理类
 *
 */
class Log
{
    /**
     * @param $msg 日志信息
     */
    public static function write($msg){
        $handler = NULL;

        if(!file_exists(LOG_FILENAME) || filesize(LOG_FILENAME)==0){
            $handler = fopen(LOG_FILENAME, 'a');
            fwrite($handler, "<?php exit();?>" . PHP_EOL);
        }
        else{

            $handler = fopen(LOG_FILENAME, 'a');
        }


        if($handler){
            $dateStr = date('Y-m-d H:i:s') . ' ';
            fwrite($handler, $dateStr . $msg . PHP_EOL);
        }

        fclose($handler);
    }
}