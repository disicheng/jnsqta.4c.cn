<?php
    namespace love4c;

    define('APPLICATION_WORK_DIR', dirname(dirname(__FILE__)));

    /**
     * 本文件实现按需进行PHP文件加载功能，在相关文件里不需要在反复进行包含依赖
     *
     * 根据项目需要，写死需要加载的文件目录，防止加载其它多余文件
     */
    class AutoLoader{
        public static function autoloader($class){
            $name = $class;

            //TODO 多个自动加载器多次加载的问题

            //首先加载配置文件
            $classFile = APPLICATION_WORK_DIR . "/config/{$name}.php";

            if(is_file($classFile)){
                require_once $classFile;
                return;
            }

            //加载config文件
            $classFile = APPLICATION_WORK_DIR . "/config/{$name}.class.php";
            if(is_file($classFile)){
                require_once $classFile;
                return;
            }

            //加载包含文件
            $classFile = APPLICATION_WORK_DIR . "/include/{$name}.class.php";
            if(is_file($classFile)){
                require_once $classFile;
                return;
            }


            //加载Controller文件
            $classFile = APPLICATION_WORK_DIR . "/controller/{$name}.class.php";
            if(is_file($classFile)){
                require_once $classFile;
                return;
            }



            $classFile = APPLICATION_WORK_DIR . "/controller/index/{$name}.class.php";
            if(is_file($classFile)){
                require_once $classFile;
                return;
            }



            $classFile = APPLICATION_WORK_DIR . "/controller/my/{$name}.class.php";
            if(is_file($classFile)){
                require_once $classFile;
                return;
            }




        }
    }

    //注册自动加载器
    spl_autoload_register('\love4c\AutoLoader::autoloader');