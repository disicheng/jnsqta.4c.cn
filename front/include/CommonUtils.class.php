<?php

//不允许单独访问本文件
if(!defined('IN_JNSQTA')){
    exit('Acessed Denied!');
}

/**
 * 工具类，所有公共工具以静态方法进行实现
 */
class CommonUtils{

    private static $zodiac = array(
        '鼠', '牛', '虎', '兔', '龙', '蛇',
        '马', '羊', '猴', '鸡', '狗', '猪'
    );

    private static $constellation = array(
        '摩羯座', '水瓶座', '双鱼座', '白羊座', '金牛座',
        '双子座', '巨蟹座', '狮子座', '处女座', '天秤座',
        '天蝎座', '射手座', '摩羯座'
    );
    private static $days = [ 20, 19, 21, 20, 21, 22, 23, 23, 23, 24, 23, 22];


    /**
     * 错误处理函数，再出现错误的时候进行日志记录
     */
    public static function errorHandler($errno, $errstr, $errfile, $errline){
        $msg = "{$errstr}  file:{$errfile}  line:{$errline}";
        Log::write($msg);
    }

    /**
     *  所有未经捕获的异常均由本函数处理，否则会触发FATAL_ERROR，导致脚本停止执行
     *
     * @param $e 异常堆栈信息
     */
    public static function exceptionHandler(Exception $e){
        $msg = $e->getMessage() . '  '.  $e->getFile() . '  ' . $e->getLine();
        Log::write($msg);
    }

    /**
     * 判断是否为微信浏览器访问
     * @return TRUE/FALSE
     */
    public static function isWechatBrowser(){
        $isWechatBrowser = FALSE;
        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';

        if(!empty($userAgent) && stripos($userAgent, 'MicroMessenger')){
            $isWechatBrowser = TRUE;
        }

        return $isWechatBrowser;
    }

    /**
     * 是否登录
     *
     *
     * TRUE/FALSE
     */
    public static function isLogin(){
        $loginStatus = FALSE;

        $loginSessionId = isset($_COOKIE[LOGINSESSID]) ? $_COOKIE[LOGINSESSID] : '';
        $expiresTime = RedisCache::getInstance()->ttl($loginSessionId); //获取SESSION过期时间
        $isLogin = TRUE;

        if(empty($loginSessionId)){//未登录或已过期
            $isLogin = FALSE;
        }
        else if($expiresTime < 0){//登录已过期或者已经被剔除(同一个手机在其他浏览器登录，之前的会被踢出)
            $isLogin = FALSE;
        }

        return $isLogin;
    }


    /**
     * 根据当前user-agent,IP地址和存放的特征码进行sessionid比对
     */
    public static function checkLoginSessionID($user, $clientSessionId){
        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $ip = self::getIP();

        $loginSessionId = md5($userAgent . $ip . $user->time .$user->key .$user->phone);

        return $loginSessionId == $clientSessionId;
    }

    /**
     * 生成随机字符串
     *
     * @param int $length
     * @return string
     */
    public static function randStr($length = 128)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);}
        return $str;
    }



    public static function uri($uri)
    {
        $uri = ltrim($uri, '/');
        if (UrlRewrite) {
            return SystemDir . $uri;
        } else {
            return SystemDir . IndexFile . '/' . $uri;
        }
    }


    public static function addslashes_str($val, $addslashes)
    {
        if ($addslashes) {
            return dbstr($val);
        } else {
            return $val;
        }
    }



    public static function gethomeurl()
    {
        if (isset($GLOBALS['homeurl'])) {
            return $GLOBALS['homeurl'];
        }
        if (!UrlRewrite) {
            $indexfile = IndexFile;
        } else {
            $indexfile = '';
        }
        $domains = explode(';', SystemDomain);
        foreach ($domains as $domain) {
            $domain = trim($domain);
            if ($domain == server_name()) {
                $GLOBALS['homeurl'] = SystemDir . $indexfile;
                return $GLOBALS['homeurl'];
            }
        }
        if (SystemDomain == '') {$GLOBALS['homeurl'] = SystemDir . $indexfile;return $GLOBALS['homeurl'];}
        $GLOBALS['homeurl'] = '//' . $domains[0] . server_port() . SystemDir . $indexfile;
        return $GLOBALS['homeurl'];
    }
    public static function str($strname, $cid = '', $return = 1)
    {
        if ($strname == '') {return false;}
        if (defined('cid') && $cid === '') {
            $cid = cid;
        } elseif ($cid === 0) {
            $cid = 0;
        } else {
            if (is_numeric($cid)) {
                $cid = intval($cid);
            } else {
                $cinfo = getchannelcache($cid);
                if ($cinfo) {
                    $cid = $cinfo['cid'];
                } else {
                    return false;
                }
            }
        }
        $strname = dbstr($strname);
        if (isset($GLOBALS['strget'][$cid][$strname])) {
            if ($return == 1) {
                return $GLOBALS['strget'][$cid][$strname];
            } else {
                echo ($GLOBALS['strget'][$cid][$strname]);
                return true;
            }
        }
        if (SiteCache) {
            $cachehash = $strname . '_' . $cid;
            $res = cacheget($cachehash, 3600 * 24 * 365, 'str');
            if ($res !== false) {
                $GLOBALS['strget'][$cid][$strname] = $res;
                if ($return == 1) {
                    return $GLOBALS['strget'][$cid][$strname];
                } else {
                    echo ($GLOBALS['strget'][$cid][$strname]);
                    return true;
                }
            }
        }
        $link = $GLOBALS['db']->one("SELECT strcid,strvalue FROM " . tableex('str') . " where strname='$strname' and strcid='$cid' limit 1;");
        if ($link) {
            $GLOBALS['strget'][$cid][$strname] = $link['strvalue'];
            if (SiteCache) {cacheset($cachehash, $link['strvalue'], 3600 * 24 * 365, 'str');}
            if ($return == 1) {
                return $GLOBALS['strget'][$cid][$strname];
            } else {
                echo ($GLOBALS['strget'][$cid][$strname]);
                return true;
            }
        }
        $GLOBALS['strget'][$cid][$strname] = false;
        return false;
    }

    public static function strset($strname, $strvalue, $cid = 0)
    {
        if ($strname == '') {
            return false;
        }
        if (is_numeric($cid)) {
            $cid = intval($cid);
        } else {
            $cinfo = getchannelcache($cid);
            if ($cinfo) {
                $cid = $cinfo['cid'];
            } else {
                return false;
            }
        }
        $strname = dbstr($strname);
        $strvalue = dbstr($strvalue);
        $query = $GLOBALS['db']->query("UPDATE " . tableex('str') . " SET strvalue='$strvalue' WHERE strname='$strname' and strcid='$cid';");
        if ($query) {
            $GLOBALS['strget'][$cid][$strname] = $strvalue;
            if (SiteCache) {
                $cachehash = $strname . '_' . $cid;
                cacheset($cachehash, $strvalue, 3600 * 24 * 365, 'str');
            }
            return true;
        } else {
            return false;
        }
    }

    public static function cut_str($str, $length = 250, $start = 0)
    {
        $charset = 'utf-8';
        if (function_exists("mb_substr")) {
            if (mb_strlen($str, $charset) <= $length) {
                return $str;
            }

            $slice = mb_substr($str, $start, $length, $charset);
        } else {
            $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
            $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
            $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
            $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
            preg_match_all($re[$charset], $str, $match);
            if (count($match[0]) <= $length) {
                return $str;
            }

            $slice = join("", array_slice($match[0], $start, $length));
        }
        return $slice;
    }
    public static function nohtml($html)
    {
        $html = preg_replace("/(\<(.*)>)/Ui", '', $html);
        $html = str_replace('<', '&lt;', $html);
        $html = str_replace('>', '&gt;', $html);
        $html = str_replace('&emsp;', '', $html);
        $html = str_replace('&nbsp;', '', $html);
        $html = str_replace("\t", '', $html);
        while (1 == 1) {
            if (stripos($html, '  ') === false) {
                break;
            } else {
                $html = str_replace('  ', ' ', $html);
            }
        }
        return $html;
    }
    public static function text($str, $length = 10)
    {
        return cut_str(nohtml($str), $length);
    }
    public static function go($url = '', $time = 0)
    {
        if (empty($url) && isset($_SERVER['HTTP_REFERER'])) {$url = htmlspecialchars($_SERVER['HTTP_REFERER']);}
        echo ("<meta http-equiv=refresh content='" . $time . "; url=" . $url . "'>");
        die();
    }
    public static function cacheget($keyname, $overtime = 604800, $keykind = '')
    {
        if (SiteCache == false) {return false;}
        $filename = cachemd5dir($keyname, $keykind);
        if (is_file($filename) && filemtime($filename) + $overtime > time()) {
            return file_get_contents($filename);
        } else {
            return false;
        }
    }
    public static function cacheset($keyname, $value, $overtime = 604800, $keykind = '')
    {
        if (SiteCache == false) {return false;}
        $filename = cachemd5dir($keyname, $keykind);
        if (!is_dir(dirname($filename))) {
            createDir(dirname($filename));
        }
        $fp = @fopen($filename, "w");
        if ($fp === false) {
            echo ($filename . ' permission denied');
            return false;
        }
        if (@fwrite($fp, $value) === false) {
            @fclose($fp);
            echo ($filename . ' permission denied');
            return false;
        }
        @fclose($fp);
        return true;
    }
    public static function cachedel($keyname, $keykind = '')
    {
        if (SiteCache == false) {return false;}
        $filename = cachemd5dir($keyname, $keykind);
        if (file_exists($filename)) {
            @unlink($filename);
        }
        return true;
    }
    public static function cachelasttime($keyname, $keykind = '')
    {
        if (SiteCache == false) {return false;}
        $filename = cachemd5dir($keyname, $keykind);
        return filemtime($filename);
    }
    public static function cachemd5dir($keyname, $keykind = '')
    {
        $md5 = md5(server_name() . server_port() . $keyname . SiteHash . UrlRewrite);
        if (!empty($keykind)) {
            $keykind .= DIRECTORY_SEPARATOR;
        }
        return CacheDir . $keykind . substr($md5, 0, 4) . DIRECTORY_SEPARATOR . substr($md5, 4, 4) . DIRECTORY_SEPARATOR . substr($md5, 8, 20) . '.html';
    }


    public static function tableex($table = '')
    {
        return TableEx . $table;
    }

    public static function _stripslashes()
    {
        if (!function_exists('get_magic_quotes_gpc') || get_magic_quotes_gpc() == 0) {return false;}
        if (isset($_GET)) {$_GET = _stripslashes_deep($_GET);}
        if (isset($_POST)) {$_POST = _stripslashes_deep($_POST);}
        if (isset($_COOKIE)) {$_COOKIE = _stripslashes_deep($_COOKIE);}
        unset($_REQUEST);
    }
    public static function _stripslashes_deep($value)
    {
        if (empty($value)) {
            return $value;
        } else {
            return is_array($value) ? array_map('_stripslashes_deep', $value) : stripslashes($value);
        }
    }
    public static function password_md5($password = '')
    {
        return md5(substr(md5($password), 0, 10) . md5($password));
    }

    public static function createDir($path)
    {
        if (!file_exists($path)) {
            createDir(dirname($path));
            if (!@mkdir($path, 0777)) {
                die(dirname($path) . ' permission denied');
            }
        }
    }
    public static function notfound()
    {
        init_route('not_found', routermobile());
    }



    public static function server_name()
    {
        if (!isset($_SERVER['UUUSERVER_NAME'])) {
            if (isset($_SERVER['HTTP_HOST'])) {
                $thisserver_names = explode(':', $_SERVER['HTTP_HOST']);
                $_SERVER['UUUSERVER_NAME'] = $thisserver_names[0];
            } else {
                $_SERVER['UUUSERVER_NAME'] = $_SERVER['SERVER_NAME'];
            }
            $_SERVER['UUUSERVER_NAME'] = strtolower($_SERVER['UUUSERVER_NAME']);
        }
        return $_SERVER['UUUSERVER_NAME'];
    }

    public static function server_port()
    {
        if (!isset($_SERVER['UUUSERVER_PORT'])) {
            if (isset($_SERVER['HTTP_HOST'])) {
                $thisserver_port = explode(':', $_SERVER['HTTP_HOST']);
                if (isset($thisserver_port[1])) {
                    $_SERVER['UUUSERVER_PORT'] = $thisserver_port[1];
                } else {
                    $_SERVER['UUUSERVER_PORT'] = '80';
                }
            } elseif (isset($_SERVER['SERVER_PORT'])) {
                $_SERVER['UUUSERVER_PORT'] = $_SERVER['SERVER_PORT'];
            } else {
                $_SERVER['UUUSERVER_PORT'] = '80';
            }
            if ($_SERVER['UUUSERVER_PORT'] == '80') {
                $_SERVER['UUUSERVER_PORT'] = '';
            } else {
                $_SERVER['UUUSERVER_PORT'] = ':' . $_SERVER['UUUSERVER_PORT'];
            }
        }
        return $_SERVER['UUUSERVER_PORT'];
    }


    public static function uridecode($uri)
    {
        $uri = str_replace('%28', '(', $uri);
        $uri = str_replace('%29', ')', $uri);
        $uri = str_replace('%7B', '{', $uri);
        $uri = str_replace('%7D', '}', $uri);
        $uri = str_replace('%5B', '[', $uri);
        $uri = str_replace('%5D', ']', $uri);
        $uri = str_replace('%2F', '/', $uri);
        $uri = str_replace('%3F', '?', $uri);
        $uri = str_replace('%3D', '=', $uri);
        $uri = str_replace('%26', '&', $uri);
        $uri = str_replace('+', '%20', $uri);
        $uri = str_replace('%25', '%', $uri);
        return $uri;
    }

    public static function echo_replace($str)
    {
        $str = str_replace("'", "\\'", $str);
        return $str;
    }

    public static function getIP()
    {
        $ip = 'unknown';

        if(getenv('HTTP_CLIENT_IP'))
        {
            $ip = getenv('HTTP_CLIENT_IP');
        }
        elseif(getenv('HTTP_X_FORWARDED_FOR'))
        {
            $ip = getenv(('HTTP_X_FORWARDED_FOR'));
        }
        elseif(getenv('REMOTE_ADDR'))
        {
            $ip = getenv('REMOTE_ADDR');
        }
        elseif(isset($_SERVER['REMOTE_ADDR']))
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return preg_match("/[\d\.]{7,15}/", $ip, $matches) ? $matches[0] : 'unknown';
    }

    public static function api($state=4000,$data=array(),$msg='')
    {
        header('Content-Type:application/json');
        $statuscode=unserialize(STATUS_CODE);
        if(empty($msg) && array_key_exists($state,$statuscode)){
            $msg=$statuscode[$state];
        }
        exit(json_encode(['state' => $state, 'data' => $data,'msg'=>$msg],JSON_UNESCAPED_UNICODE));
    }

    //验证身份证
    public static function is_idcard( $id )
    {
        $id = strtoupper($id);
        $regx = "/(^\d{15}$)|(^\d{17}([0-9]|X)$)/";
        $arr_split = array();
        if(!preg_match($regx, $id))
        {
            return FALSE;
        }
        if(15==strlen($id)) //检查15位
        {
            $regx = "/^(\d{6})+(\d{2})+(\d{2})+(\d{2})+(\d{3})$/";

            @preg_match($regx, $id, $arr_split);
            //检查生日日期是否正确
            $dtm_birth = "19".$arr_split[2] . '/' . $arr_split[3]. '/' .$arr_split[4];
            if(!strtotime($dtm_birth))
            {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        else      //检查18位
        {
            $regx = "/^(\d{6})+(\d{4})+(\d{2})+(\d{2})+(\d{3})([0-9]|X)$/";
            @preg_match($regx, $id, $arr_split);
            $dtm_birth = $arr_split[2] . '/' . $arr_split[3]. '/' .$arr_split[4];
            if(!strtotime($dtm_birth)) //检查生日日期是否正确
            {
                return FALSE;
            }
            else
            {
                //检验18位身份证的校验码是否正确。
                //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
                $arr_int = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
                $arr_ch = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
                $sign = 0;
                for ( $i = 0; $i < 17; $i++ )
                {
                    $b = (int) $id{$i};
                    $w = $arr_int[$i];
                    $sign += $b * $w;
                }
                $n = $sign % 11;
                $val_num = $arr_ch[$n];
                if ($val_num != substr($id,17, 1))
                {
                    return FALSE;
                }
                else
                {
                    return TRUE;
                }
            }
        }

    }

    //校验身份证有效期
    public static function verifyInDate($valid_date)
    {
        $ret = false;

        $timeStr = explode('-', $valid_date);
        $startTime = implode(explode('.', $timeStr[0]));
        $endTime = implode(explode('.', $timeStr[1]));
        $nowTime = date('Ymd');

        if (intval($nowTime) <= intval($endTime) && intval($nowTime) >= intval($startTime)) {
            $ret = true;
        } else {
            $ret = false;
        }

        return $ret;
    }



    /**
     * @desc 验证码发送接口
     */
    public static function sendSMS($code, $phone)
    {
        $client = new TopClient();
        $client->appkey = SMS_API_KEY;
        $client->secretKey = SMS_SECRET_KEY;

        $req = new AlibabaAliqinFcSmsNumSendRequest();
        $req->setExtend($phone);
        $req->setSmsType('normal');
        $req->setSmsFreeSignName('第四城');
        $req->setSmsParam("{\"code\":\"{$code}\",\"product\":\"alidayu\"}");
        $req->setRecNum($phone);
        $req->setSmsTemplateCode("SMS_170156219");
        $resp = $client->execute($req);

        var_dump($resp);

    //    $url = "https://api.cdwed.cn/v1/sms/send";
    //    $ch = curl_init();
    //    curl_setopt($ch, CURLOPT_URL, $url);
    //    curl_setopt($ch, CURLOPT_HEADER, 0);
    //    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //    curl_setopt($ch, CURLOPT_POST, 1);
    //    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
    //    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'apikey: ' . $apikey, 'Content-Type: application/json'));
    //    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    //    $data = curl_exec($ch);
    //    curl_close($ch);
    //    return $data;
    }

    /**
     * @desc 加密解密函数
     */
    public static function encrypt($string, $operation = 'E', $key = ENCRYPT_KEY)
    {
        $key = md5($key);
        $key_length = strlen($key);
        $string = $operation == 'D' ? base64_decode($string) : substr(md5($string . $key), 0, 8) . $string;
        $string_length = strlen($string);
        $rndkey = $box = array();
        $result = '';
        for ($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($key[$i % $key_length]);
            $box[$i] = $i;
        }
        for ($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for ($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }
        if ($operation == 'D') {
            if (substr($result, 0, 8) == substr(md5(substr($result, 8) . $key), 0, 8)) {
                return substr($result, 8);
            } else {
                return '';
            }
        } else {
            return str_replace('=', '', base64_encode($result));
        }
    }


    public static function getCurrentUrl()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        return $url;
    }

    /**
     * 返回当前主机Host
     * @return string
     */
    public static function getCurrentHost()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = $protocol . $_SERVER['HTTP_HOST'] ;
        return $url;
    }

    /**
     * 用*替换名字后续字符
     *
     * @param $user_name
     * @return string
     */
    public static function starReplace($user_name)
    {
        $strlen = mb_strlen($user_name, 'utf-8');
        $firstStr = mb_substr($user_name, 0, 1, 'utf-8');
        $startStr = $firstStr . '*';

        if($strlen > 2)
        {
            $startStr .= '*';
        }
        return $startStr;
    }

    /**
     * 根据生日计算生肖
     * @param 生日 1980-12-31
     * @return 返回生肖
     */
    public static function chineseZodiac($birthday){
        list($year ,$month, $day) = explode('/',$birthday);
        $year = intval($year);
        $month = intval($month);
        $day = intval($day);

        $index = ($year - 1900) % 12;
        return self::$zodiac[$index];
    }

    /**
     * 根据生日星座
     *
     * @param $birthday 1980-12-31

     * @return 星座中文
     */
    public static function constellationName($birthday){

        list($year ,$month, $day) = explode('/',$birthday);
        $year = intval($year);
        $month = intval($month);
        $day = intval($day);

        return $day < self::$days[$month - 1] ?
                    self::$constellation[$month - 1] : self::$constellation[$month];
    }

    /**
     * 身份是否过有效期
     * @param $validDate 格式为：2011.08.02-2021.08.02 ，腾讯云识别格式
     * @return  TRUE/FALSE
     */
    public static function idcardValidate($validDate){
        $validDate = explode('-', $validDate);
        $isValid = FALSE;

        if(sizeof($validDate) == 2){
            $validDate[1] = str_replace('.','-', $validDate[1]);

            if(strtotime($validDate[1]) + 24 * 60 *60 > time()){//增加24小时，表示当日内有效
                $isValid = TRUE;
            }
        }

        return $isValid;

    }
}