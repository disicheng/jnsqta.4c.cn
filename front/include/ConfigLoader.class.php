<?php

/**
 * 配置文件加载器，将配置文件内容加载成Array
 */
class ConfigLoader
{
    private static $configCache = [];
    /**
     * 加载配置文件并返回Array
     *
     * @param 文件绝对路径
     * @return Array，如果没有数据或加载失败返回空数据组array()
     */
    public static function load($fileName){
        //优先加载缓存
        if(!empty(self::$configCache)){
            return self::$configCache;
        }

        $config = [];
        if(file_exists($fileName)) {
            $content = file($fileName);
            $total = count($content);
            $cunt = 0;
            while($cunt < $total){
                //过滤所有空白字符
                //TODO 注意中文乱码问题需要开启/u模式，识别UTF-8字符
                $line = preg_replace('/\t|[\r\n|\r|\n| | ]/u','',$content[$cunt]);

                if(strstr($line, '=')){
                    $lineArray = explode('=', $line);
                    if(count($lineArray) == 2){
                        $configArray = explode(':', $lineArray[1]);

                        if(count($configArray) == 2){
                            $config["{$lineArray[0]}"] = ['state' => "{$configArray[0]}",
                                'msg' => "{$configArray[1]}"];
                        }
                    }
                }
                $cunt++;
            }
        }

        return $config;
    }
}