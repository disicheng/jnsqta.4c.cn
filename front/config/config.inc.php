<?php

//不允许单独访问本文件
if(!defined('IN_JNSQTA')){
    exit('Acessed Denied!');
}
define('SystemRoot', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
require_once SystemRoot . 'include/AutoLoader.inc.php';
require_once SystemRoot . 'config/dbconfig.inc.php';


/**
 * 系统错误初始化
 */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('log_errors',TRUE);
ini_set('error_log', SystemRoot . 'logs/system_error');
//set_error_handler(['CommonUtils','errorHandler']); //E_ALL E_NOTICE; E_WARNING;以及用户自定义级别错误进行日志处理
//set_exception_handler(['CommonUtils','exceptionHandler']); //设置未被捕获异常处理器

date_default_timezone_set('Asia/Shanghai');

define('ACCESS_DENIED', 'Acessed Denied!');
define('RESPONSE_STATUS_FILE_NAME', SystemRoot . 'include/reponse_status.properties.php');
define('LOG_FILENAME', SystemRoot . 'logs/errorlog.php');