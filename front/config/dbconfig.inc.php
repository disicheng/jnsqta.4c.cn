<?php
//数据库配置
const DB_CONFIG = [
    'driver' => 'mysql',
    'host' => '127.0.0.1',
    'port' => '3306',
    'username' => 'root',
    'password' => 'root',
    'dbname' => 'jnsqta',
    'charset' => 'utf8mb4',
    'pconnect' => false,
    'time_out' => 3,
    'prefix' => 'jnsqta_',
    'throw_exception' => true
];

define('NOTICE_TABLE', DB_CONFIG['prefix'].'notice');
define('PROPOSAL_TABLE', DB_CONFIG['prefix'].'proposal');
define('TYPE_TABLE', DB_CONFIG['prefix'].'type');
define('STATUS_TABLE', DB_CONFIG['prefix'].'status');
define('CATEGORY_TABLE', DB_CONFIG['prefix'].'category');

