<?php

/** 定义系统使用的常量
  *
  * @author WuYong
  * @copyright 成都第四城文化传播有限责任公司
  *
  */
class SystemCode
{
    const SELECTION_PROPOSAL = 1; //精选提案

    const PERSONAL_PROPOSAL = 1; //个人提案
    const ORGANIZATION_PROPOSAL = 2; //组织提案

    const STATUS_WAIT_PROCESS = 1; //待受理
    const STATUS_RES_PROCESS = 2; //院落受理
    const STATUS_REJECT = 3; //驳回

    const STATUS_SUBMIT_COMMUNITY = 4; //报送社区议事会
    const STATUS_COMMUNITY_PROCESS = 5; //社区受理

    const STATUS_SUBMIT_STREET = 6; //报送街道
    const STATUS_STREET_PROCESS = 7; //街道受理

    const STATUS_SUBMIT_DISTRICT = 8; //报送区级部门受理
    const STATUS_DISTRICT_PROCESS = 9; //报送区级部门受理

}