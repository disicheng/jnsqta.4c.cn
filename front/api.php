<?php
    session_start();
    define('IN_JNSQTA', TRUE);
    require_once 'config/config.inc.php';

    header("Content-type: text/html; charset=UTF-8");

    $action = isset($_GET['act']) ?  strtolower($_GET['act']) : '';

    Router::registeRouter('getnotice','GetNotice');
    Router::registeRouter('getselection','GetSelection');
    Router::registeRouter('viewproposal','ViewProposal');
    Router::registeRouter('dosubmit','SubmitProposal');
    Router::registeRouter('search','Search');

    Router::registeRouter('myroposal','MyProposal');


    $router = Router::getRouter();
    //检查接口非法调用
    if(!key_exists($action, $router)){
        exit(ACCESS_DENIED);
    }

    Router::route($action);//执行路由










