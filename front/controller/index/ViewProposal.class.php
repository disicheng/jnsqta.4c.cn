<?php

/**
 * 查看提案
 */
class ViewProposal extends ApiController{

    /**
     * @return mixed response_status.properties中定义的状态
     */
    public function execute()
    {
        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        $result = [];

        if($id > 0){
            $result = $this->db->table(PROPOSAL_TABLE)->where('id','=', $id)->first();
            $this->adujstProposal($result);
        }


        return $result;
    }
}
