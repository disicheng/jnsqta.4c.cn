<?php

/**
 * 获取公告
 */
    class GetNotice extends ApiController{

        /**
         * @return mixed response_status.properties中定义的状态
         */
        public function execute()
        {
            //获取最后一条公告
            $notice = $this->db->select('title')
                            ->table(NOTICE_TABLE)
                            ->orderBy('id','desc')->first();

            return $notice;
        }
    }