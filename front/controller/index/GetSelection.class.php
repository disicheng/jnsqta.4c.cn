<?php

/**
 * 精选提案
 */
class GetSelection extends ApiController{

    /**
     * @return mixed response_status.properties中定义的状态
     */
    public function execute()
    {
        $list = $this->db->select('reason','post_time','category','status','organization','type')
                  ->table(PROPOSAL_TABLE)->where('selection','=', SystemCode::SELECTION_PROPOSAL)
                  ->where('status','<>', SystemCode::STATUS_WAIT_PROCESS)
                  ->orderBy('posttime','DESC')->limit(10)->get();

        if(!empty($list)){
            foreach ($list as $index => $value){
                $list[$index]['category'] = $this->sysConfig['category'][$value['category']];
                $list[$index]['type'] = $this->sysConfig['type'][$value['type']];
                $list[$index]['status'] = $this->sysConfig['status'][$value['status']];
            }
        }

        return $list;

    }
}
