<?php

/**
 * 提交提案
 */
    class SubmitProposal extends ApiController{

        /**
         * @return mixed response_status.properties中定义的状态
         */
        public function execute()
        {
            $type = isset($_POST['type']) ? htmlentities($_POST['type']) : SystemCode::PERSONAL_PROPOSAL;//提案类型
            $proposer = isset($_POST['proposer']) ? htmlentities($_POST['proposer']) : '';//提案人
            $seconder = isset($_POST['seconder']) ? htmlentities($_POST['seconder']) : '';//附议人
            $phone = isset($_POST['phone']) ? htmlentities($_POST['phone']) : '';//联系电话
            $org = isset($_POST['organization']) ? htmlentities($_POST['organization']) : '';//单位/组织
            $community = isset($_POST['community']) ? htmlentities($_POST['community']) : '';//所属社区
            $residential = isset($_POST['residential']) ? htmlentities($_POST['residential']) : '';//所属小区
            $category = isset($_POST['category']) ? htmlentities($_POST['category']) : '';//提案类型
            $reason = isset($_POST['reason']) ? htmlentities($_POST['reason']) : '';//案由
            $content = isset($_POST['content']) ? htmlentities($_POST['content']) : '';//内容及建议
            $postTime = time();


            if($type == SystemCode::ORGANIZATION_PROPOSAL){//组织提案
                if(empty($org) || mb_strlen($org,'utf-8') < 2){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'提案单位/组织不能为空']));
                }

                if(empty($phone) || !preg_match('/^1[0-9]{10}$/', $phone)){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'请输入有效的电话号码']));
                }

                if(empty($category) || strpos($category, '请选择') === 0){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'请选择提案分类']));
                }

                if(empty($community) || strpos($community, '请选择') === 0){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'请选择提案小区']));
                }

                if(empty($reason) || mb_strlen($reason, 'utf-8') < 5){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'案由不能少于5个字']));
                }
                if(empty($content) || mb_strlen($reason, 'utf-8') < 50){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'提案内容不能少于50字']));
                }
            }


            if($type == SystemCode::PERSONAL_PROPOSAL){//个人提案

                if(empty($proposer) || mb_strlen($proposer,'utf-8') < 2){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'提案人至少需要输入2个字']));
                }

                if(empty($seconder) || mb_strlen($seconder,'utf-8') < 2){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'附议人至少需要2个字']));
                }


                if(empty($phone) || !preg_match('/^1[0-9]{10}$/', $phone)){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'请输入有效的电话号码']));
                }

                if(empty($category) || strpos($category, '请选择') === 0){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'请选择提案分类']));
                }

                if(empty($community) || strpos($community, '请选择') === 0){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'请选择提案小区']));
                }

                if(empty($reason) || mb_strlen($reason, 'utf-8') < 5){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'案由不能少于5个字']));
                }
                if(empty($content) || mb_strlen($content, 'utf-8') < 30){
                    exit(json_encode(['state'=>FALSE, 'msg'=>'提案内容不能少于30字']));
                }
            }

            try{
                $this->db->table(PROPOSAL_TABLE)->insert([
                    'reason' => $reason, 'type' => $type, 'category' => $category, 'for_community'=>$community ,
                    'for_residential' => $residential, 'proposer' => $proposer, 'seconder' => $seconder,
                    'contact_phone'=>$phone, 'content' => $content, 'posttime' => $postTime, 'cid'=>3 , 'adminuid'=>1,
                    'status'=>SystemCode::STATUS_WAIT_PROCESS
                ]);

                exit(json_encode(['state'=>TRUE, 'msg'=>'提交成功，请等待处理！']));
            }catch (Exception $e){
                Log::write($e->getMessage() . " 提交提案失败，提案人：{$proposer} 组织：{$org} 联系电话：{$phone}");
                exit(['state'=>FALSE, 'msg'=>'提交失败，请重试！']);
            }
        }
    }
