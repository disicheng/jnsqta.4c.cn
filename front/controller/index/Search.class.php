<?php

/**
 * 按照案由或者内容关键词搜索
 */
class Search extends ApiController{

    /**
     * @return mixed response_status.properties中定义的状态
     */
    public function execute()
    {
        $keyword = isset($_POST['keyword']) ? htmlentities($_POST['keyword']) : '';
        $result = [];

        $sql = "SELECT `reason`, `post_time`, `category`, `status`, `organization`, `type` FROM " . PROPOSAL_TABLE .
                " WHERE reason LIKE :keyword";

        $pdo = $this->db->getPdo();
        $state = $pdo->prepare($sql);
        $state->bindValue(':keyword',  "%{$keyword}%");
        $state->execute();
        $result = $state->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($result)){
            foreach ($result as $index => $value){
                $result[$index]['category'] = $this->sysConfig['category'][$value['category']];
                $result[$index]['type'] = $this->sysConfig['type'][$value['type']];
                $result[$index]['status'] = $this->sysConfig['status'][$value['status']];
            }
        }

        return $result;
    }
}
