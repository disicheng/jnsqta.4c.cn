<?php

/**
 * 管理路由配置并执行路由操作
 */
class Router{
    private static $router = [];

    /*
     * 执行路由
     * @param $action
     */
    public static function route($action)
    {
        //token检查
        $token = isset($_POST['token']) ? $_POST['token'] : '';

//        if(!in_array($action, $unCheck) && $token !== API_TOKEN){
//            exit(ACCESS_DENIED);
//        }


        $controller = new self::$router[$action];

        echo json_encode($controller->execute());
    }


    public static function registeRouter($action, $className){
        self::$router[$action] = $className;
    }

    public static function getRouter(){
        return self::$router;
    }

}
