<?php


/**
 * API控制器
 */
abstract class ApiController
{
    protected $responseStatus = NULL;
    protected $db = NULL;
    protected $sysConfig = NULL; //系统变量配置


    /**
     * @return mixed response_status.properties中定义的状态
     */
    abstract public function execute();

    public function __construct()
    {
        $this->db = Database::getInstance();
        $this->sysConfig = $this->getConfig();//加载金币等系统配置

        //加载相应状态配置文件
        $this->responseStatus = ConfigLoader::load(RESPONSE_STATUS_FILE_NAME);
    }


    /**
     * @description: 获取系统金币配置
     * @param {type}
     * @return:
     */
    protected function getConfig()
    {
        $config['category'] = [];
        $config['status'] = [];
        $config['type'] = [];

        $data = $this->db->table(CATEGORY_TABLE)->select('id', 'category_name')->get();
        $this->covertArrayToIndex($config['category'], $data, ['id', 'category_name']);

        $data = $this->db->table(STATUS_TABLE)->select('id', 'status_name')->get();
        $this->covertArrayToIndex($config['status'], $data, ['id','status_name']);

        $data = $this->db->table(TYPE_TABLE)->select('id', 'type_name')->get();
        $this->covertArrayToIndex($config['type'], $data,['id','type_name']);

        return $config;
    }


    private function covertArrayToIndex(&$array, $list, $keys){
        foreach ($list as $value){
            $array[$value[$keys[0]]] = $value[$keys[1]];
        }
    }

    protected  function adujstProposal($proposal){
        $status = $proposal['status'];
        $proposal['category'] = $this->sysConfig['category'][$proposal['category']];
        $proposal['type'] = $this->sysConfig['type'][$proposal['type']];
        $proposal['status'] = $this->sysConfig['status'][$proposal['status']];


        if($status == SystemCode::STATUS_WAIT_PROCESS ||
            ($status == SystemCode::STATUS_RES_PROCESS && empty($proposal['res_audit_mind']))){//待处理或院落没有给出相关意见
            unset($proposal['community_audit_mind']);
            unset($proposal['community_audit_time']);
            unset($proposal['street_audit_mind']);
            unset($proposal['street_audit_time']);
            unset($proposal['district_audit_mind']);
            unset($proposal['district_audit_time']);
            unset($proposal['res_audit_mind']);
            unset($proposal['res_audit_time']);
            unset($proposal['res_should_num']);
            unset($proposal['res_actual_num']);
            unset($proposal['res_agree_num']);
            unset($proposal['for_street']);
            unset($proposal['selection']);
            unset($proposal['organization']);
        }

        if($status == SystemCode::STATUS_COMMUNITY_PROCESS
            && empty($proposal['community_audit_mind'])){//社区受理，还未有结果时
            unset($proposal['community_audit_mind']);
            unset($proposal['community_audit_time']);
            unset($proposal['street_audit_mind']);
            unset($proposal['street_audit_time']);
            unset($proposal['district_audit_mind']);
            unset($proposal['district_audit_time']);
            unset($proposal['for_street']);
            unset($proposal['selection']);
            unset($proposal['organization']);
        }

        if($status == SystemCode::STATUS_STREET_PROCESS
            && empty($proposal['street_audit_mind'])){//街道受理，还未有结果时
            unset($proposal['street_audit_mind']);
            unset($proposal['street_audit_time']);
            unset($proposal['district_audit_mind']);
            unset($proposal['district_audit_time']);
            unset($proposal['for_street']);
            unset($proposal['selection']);
            unset($proposal['organization']);
        }

        if($status = SystemCode::STATUS_DISTRICT_PROCESS
            && empty($proposal['district_audit_mind'])){//区级部门受理，还未有结果时
            unset($proposal['district_audit_mind']);
            unset($proposal['district_audit_time']);
            unset($proposal['for_street']);
            unset($proposal['selection']);
            unset($proposal['organization']);
        }

        unset($proposal['cid']);
        unset($proposal['adminuid']);
        unset($proposal['serial_number']);

        return $proposal;
    }


}