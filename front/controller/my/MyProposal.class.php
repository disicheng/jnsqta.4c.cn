<?php

/**
 * 我的提案
 */
class MyProposal extends ApiController{

    /**
     * @return mixed response_status.properties中定义的状态
     */
    public function execute()
    {
        $phone = isset($_GET['phone']) ? htmlentities($_GET['phone']) : '';
        $result = [];

        if(!empty($phone)){
           $result = $this->db->table(PROPOSAL_TABLE)
                          ->where('contact_phone','=', $phone)->orderBy('id','DESC')->get();
           foreach ($result as $index => $value){
               $result[$index] = $this->adujstProposal($value);
           }
        }

        return $result;
    }
}
