# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.25)
# Database: jnsqta
# Generation Time: 2019-10-20 16:38:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table jnsqta_admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_admin`;

CREATE TABLE `jnsqta_admin` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `kind` tinyint(2) NOT NULL,
  `psd` varchar(32) NOT NULL,
  `lasttime` varchar(12) DEFAULT NULL,
  `lastip` varchar(20) DEFAULT NULL,
  `nickname` varchar(20) DEFAULT NULL,
  `alevel` tinyint(2) DEFAULT NULL,
  `power` text,
  `asetting` text,
  `ucmsid` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_admin` WRITE;
/*!40000 ALTER TABLE `jnsqta_admin` DISABLE KEYS */;

INSERT INTO `jnsqta_admin` (`id`, `username`, `kind`, `psd`, `lasttime`, `lastip`, `nickname`, `alevel`, `power`, `asetting`, `ucmsid`)
VALUES
	(1,'admin',0,'f4abced18570f6e4b7a0802d493b55df',NULL,NULL,'管理员',3,'{\"b\":{\"1\":1,\"2\":1},\"alevel\":3}',NULL,'');

/*!40000 ALTER TABLE `jnsqta_admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_category`;

CREATE TABLE `jnsqta_category` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `posttime` int(10) DEFAULT '0',
  `cid` int(8) DEFAULT '0',
  `adminuid` int(8) DEFAULT '0',
  `category_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_category` WRITE;
/*!40000 ALTER TABLE `jnsqta_category` DISABLE KEYS */;

INSERT INTO `jnsqta_category` (`id`, `posttime`, `cid`, `adminuid`, `category_name`)
VALUES
	(1,1571561747,12,1,'建设类'),
	(2,1571561758,12,1,'整治类'),
	(3,1571561767,12,1,'公共安全类'),
	(4,1571561777,12,1,'社区服务类'),
	(5,1571561793,12,1,'自治共治类'),
	(6,1571561817,12,1,'其他');

/*!40000 ALTER TABLE `jnsqta_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_channel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_channel`;

CREATE TABLE `jnsqta_channel` (
  `cid` int(5) NOT NULL AUTO_INCREMENT,
  `fid` int(5) NOT NULL DEFAULT '0',
  `cname` varchar(50) DEFAULT NULL,
  `ckind` tinyint(2) NOT NULL,
  `ifshow` tinyint(1) DEFAULT NULL,
  `ifshownav` tinyint(1) DEFAULT NULL,
  `ifshowadmin` tinyint(1) DEFAULT NULL,
  `ifshowleft` tinyint(1) DEFAULT NULL,
  `newwindow` tinyint(1) DEFAULT NULL,
  `corder` int(5) DEFAULT NULL,
  `csetting` text,
  `cvalue` text,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_channel` WRITE;
/*!40000 ALTER TABLE `jnsqta_channel` DISABLE KEYS */;

INSERT INTO `jnsqta_channel` (`cid`, `fid`, `cname`, `ckind`, `ifshow`, `ifshownav`, `ifshowadmin`, `ifshowleft`, `newwindow`, `corder`, `csetting`, `cvalue`)
VALUES
	(1,0,'公告发布',2,1,1,1,1,0,5,'{\"channel_url\":\"\\/(cid)\\/\",\"template\":\"page.php\",\"channel_urlcache\":0,\"channel_pageurl\":\"\\/(cid)\\/page_(page).html\",\"templatelist\":\"list.php\",\"channel_pageurlcache\":0,\"channel_articleurl\":\"\\/(cid)\\/(id).html\",\"templatearticle\":\"article.php\",\"channel_articleurlcache\":0,\"articletable\":\"jnsqta_notice\",\"temppage_size\":15,\"temppage_order\":\"id desc\",\"listadminuid\":1,\"cnote\":\"\",\"channel_domain\":\"\",\"otherpage\":\"[]\",\"listnoadd\":0,\"listnoedit\":0,\"listnodel\":0,\"listcopy\":0,\"listarticlemove\":0,\"listshowadminname\":0,\"listfidshow\":0}',''),
	(2,0,'提案管理',5,1,1,1,1,NULL,10,'[]','/index.php/3/'),
	(3,2,'所有提案',2,1,1,1,1,0,15,'{\"channel_url\":\"\\/(cid)\\/\",\"template\":\"list.php\",\"channel_urlcache\":0,\"channel_pageurl\":\"\\/(cid)\\/page_(page).html\",\"templatelist\":\"list.php\",\"channel_pageurlcache\":0,\"channel_articleurl\":\"\\/(cid)\\/(id).html\",\"templatearticle\":\"article.php\",\"channel_articleurlcache\":0,\"articletable\":\"jnsqta_proposal\",\"temppage_size\":15,\"temppage_order\":\"id desc\",\"listadminuid\":1,\"cnote\":\"\",\"channel_domain\":\"\",\"otherpage\":\"[]\",\"listnoadd\":0,\"listnoedit\":0,\"listnodel\":0,\"listcopy\":0,\"listarticlemove\":0,\"listshowadminname\":0,\"listfidshow\":0}',''),
	(12,8,'分类设置',2,1,1,1,1,0,55,'{\"channel_url\":\"\\/(cid)\\/\",\"template\":\"list.php\",\"channel_urlcache\":0,\"channel_pageurl\":\"\\/(cid)\\/page_(page).html\",\"templatelist\":\"list.php\",\"channel_pageurlcache\":0,\"channel_articleurl\":\"\\/(cid)\\/(id).html\",\"templatearticle\":\"article.php\",\"channel_articleurlcache\":0,\"articletable\":\"jnsqta_category\",\"temppage_size\":15,\"temppage_order\":\"id desc\",\"listadminuid\":1,\"cnote\":\"\",\"channel_domain\":\"\",\"otherpage\":\"[]\",\"listnoadd\":0,\"listnoedit\":0,\"listnodel\":0,\"listcopy\":0,\"listarticlemove\":0,\"listshowadminname\":0,\"listfidshow\":0}',''),
	(13,8,'提案类型',2,1,1,1,1,0,60,'{\"channel_url\":\"\\/(cid)\\/\",\"template\":\"list.php\",\"channel_urlcache\":0,\"channel_pageurl\":\"\\/(cid)\\/page_(page).html\",\"templatelist\":\"list.php\",\"channel_pageurlcache\":0,\"channel_articleurl\":\"\\/(cid)\\/(id).html\",\"templatearticle\":\"article.php\",\"channel_articleurlcache\":0,\"articletable\":\"jnsqta_type\",\"temppage_size\":15,\"temppage_order\":\"id desc\",\"listadminuid\":1,\"cnote\":\"\",\"channel_domain\":\"\",\"otherpage\":\"[]\",\"listnoadd\":0,\"listnoedit\":0,\"listnodel\":0,\"listcopy\":0,\"listarticlemove\":0,\"listshowadminname\":0,\"listfidshow\":0}',''),
	(14,8,'所属街道',2,1,1,1,1,0,65,'{\"channel_url\":\"\\/(cid)\\/\",\"template\":\"list.php\",\"channel_urlcache\":0,\"channel_pageurl\":\"\\/(cid)\\/page_(page).html\",\"templatelist\":\"list.php\",\"channel_pageurlcache\":0,\"channel_articleurl\":\"\\/(cid)\\/(id).html\",\"templatearticle\":\"article.php\",\"channel_articleurlcache\":0,\"articletable\":\"jnsqta_street\",\"temppage_size\":15,\"temppage_order\":\"id desc\",\"listadminuid\":1,\"cnote\":\"\",\"channel_domain\":\"\",\"otherpage\":\"[]\",\"listnoadd\":0,\"listnoedit\":0,\"listnodel\":0,\"listcopy\":0,\"listarticlemove\":0,\"listshowadminname\":0,\"listfidshow\":0}',''),
	(15,8,'提案状态',2,1,1,1,1,0,70,'{\"channel_url\":\"\\/(cid)\\/\",\"template\":\"list.php\",\"channel_urlcache\":0,\"channel_pageurl\":\"\\/(cid)\\/page_(page).html\",\"templatelist\":\"list.php\",\"channel_pageurlcache\":0,\"channel_articleurl\":\"\\/(cid)\\/(id).html\",\"templatearticle\":\"article.php\",\"channel_articleurlcache\":0,\"articletable\":\"jnsqta_status\",\"temppage_size\":15,\"temppage_order\":\"aorder asc\",\"listadminuid\":1,\"cnote\":\"\",\"channel_domain\":\"\",\"otherpage\":\"[]\",\"listnoadd\":0,\"listnoedit\":0,\"listnodel\":0,\"listcopy\":0,\"listarticlemove\":0,\"listshowadminname\":0,\"listfidshow\":0}',''),
	(8,0,'系统配置',5,1,1,1,1,NULL,40,'[]','/index.php/12/'),
	(10,14,'社区设置',2,1,1,1,1,0,50,'{\"channel_url\":\"\\/(cid)\\/\",\"template\":\"list.php\",\"channel_urlcache\":0,\"channel_pageurl\":\"\\/(cid)\\/page_(page).html\",\"templatelist\":\"list.php\",\"channel_pageurlcache\":0,\"channel_articleurl\":\"\\/(cid)\\/(id).html\",\"templatearticle\":\"article.php\",\"channel_articleurlcache\":0,\"articletable\":\"jnsqta_community\",\"temppage_size\":15,\"temppage_order\":\"id desc\",\"listadminuid\":1,\"cnote\":\"\",\"channel_domain\":\"\",\"otherpage\":\"[]\",\"listnoadd\":0,\"listnoedit\":0,\"listnodel\":0,\"listcopy\":0,\"listarticlemove\":0,\"listshowadminname\":0,\"listfidshow\":0}','');

/*!40000 ALTER TABLE `jnsqta_channel` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_community
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_community`;

CREATE TABLE `jnsqta_community` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `posttime` int(10) DEFAULT '0',
  `cid` int(8) DEFAULT '0',
  `adminuid` int(8) DEFAULT '0',
  `name` varchar(250) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_community` WRITE;
/*!40000 ALTER TABLE `jnsqta_community` DISABLE KEYS */;

INSERT INTO `jnsqta_community` (`id`, `posttime`, `cid`, `adminuid`, `name`, `fid`)
VALUES
	(1,1571532453,10,1,'金鱼街社区',1),
	(2,1571563052,10,1,'九里堤北路社区',5);

/*!40000 ALTER TABLE `jnsqta_community` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_level`;

CREATE TABLE `jnsqta_level` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `posttime` int(10) DEFAULT '0',
  `cid` int(8) DEFAULT '0',
  `adminuid` int(8) DEFAULT '0',
  `level_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table jnsqta_moudle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_moudle`;

CREATE TABLE `jnsqta_moudle` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `cid` int(5) DEFAULT NULL,
  `fid` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `minfo` varchar(100) DEFAULT NULL,
  `mcontent` text,
  `mkind` int(3) DEFAULT NULL,
  `morder` int(6) DEFAULT NULL,
  `ifcreated` tinyint(2) DEFAULT NULL,
  `ifshow` tinyint(2) DEFAULT NULL,
  `ifonly` tinyint(2) DEFAULT NULL,
  `ifadmin` tinyint(2) DEFAULT NULL,
  `ifmore` tinyint(2) DEFAULT NULL,
  `hide` tinyint(2) DEFAULT NULL,
  `ifshowtemp` text,
  `strarray` varchar(250) DEFAULT NULL,
  `msetting` text,
  `strdefault` varchar(250) DEFAULT NULL,
  `mfunction` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_moudle` WRITE;
/*!40000 ALTER TABLE `jnsqta_moudle` DISABLE KEYS */;

INSERT INTO `jnsqta_moudle` (`id`, `cid`, `fid`, `mname`, `minfo`, `mcontent`, `mkind`, `morder`, `ifcreated`, `ifshow`, `ifonly`, `ifadmin`, `ifmore`, `hide`, `ifshowtemp`, `strarray`, `msetting`, `strdefault`, `mfunction`)
VALUES
	(1,1,'','title','公告标题','',1,5,1,1,0,0,NULL,0,'',NULL,'{\"filterhtml\":1,\"adminsearch\":1}','',NULL),
	(2,1,'','content','公告内容','',3,10,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','','height:500px;width:500px'),
	(3,3,'','serial_number','提案编号','',1,5,1,1,0,0,1,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(4,3,'','reason','案由','',1,15,1,1,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(5,3,'','type','提案类型','',11,20,1,1,0,0,0,0,'','13|type_name','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(6,3,'','category','提案分类','',11,25,1,1,0,0,0,0,'','12|category_name','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(7,3,'','for_community','所属社区','',17,28,1,1,0,0,0,0,'','10|fid','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(8,3,'','proposer','提案人','',1,40,1,0,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(9,3,'','seconder','附议人','',1,45,1,0,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(10,3,'','for_residential','所属小区','',1,35,1,1,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(11,3,'','contact_phone','联系电话','',1,50,1,0,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(12,3,'','content','内容及建议','',2,55,1,0,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(31,12,'','category_name','分类名称','',1,5,1,1,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(14,3,'','status','提案状态','',11,155,1,1,0,0,0,0,'','15|status_name','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(18,3,'','post_time','提案时间','',9,70,1,0,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','now',NULL),
	(16,10,'','name','社区名字','',1,5,1,1,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(17,10,'','fid','所属街道','',11,10,1,1,0,0,0,0,'','14|name','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','0',''),
	(19,3,'','community_audit_mind','社区审核意见','',2,100,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(20,3,'','community_audit_time','社区审核时间','',9,105,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(21,3,'','street_audit_mind','街道审核意见','',2,110,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(22,3,'','street_audit_time','街道审核时间','',9,115,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(23,3,'','district_audit_mind','区级审核意见','',2,120,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(24,3,'','district_audit_time','区级审核时间','',9,125,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','',''),
	(25,3,'','res_audit_mind','院落审核意见','',2,75,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(26,3,'','res_audit_time','院落审核时间','',9,95,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(27,3,'','res_should_num','院落应到人数','',7,80,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(28,3,'','res_actual_num','院落实到人数','',7,85,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(29,3,'','res_agree_num','院落同意人数','',7,90,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"0\",\"lenmax\":\"0\",\"filterhtml\":\"1\"}','',''),
	(32,13,'','type_name','类型名称','',1,5,1,1,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(35,14,'','name','街道名称','',1,5,1,1,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(36,15,'','status_name','状态名称','',1,5,1,1,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','',NULL),
	(37,15,'','aorder','排序','',7,10,1,0,0,0,NULL,0,'',NULL,'{\"filterhtml\":1}','1',NULL),
	(38,3,'','selection','是否精选','',26,160,1,1,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','0',''),
	(39,3,'','organization','单位/组织','',1,26,1,0,0,0,0,0,'','','{\"regular\":\"\",\"lenmin\":\"\",\"lenmax\":\"\",\"filterhtml\":\"1\"}','','');

/*!40000 ALTER TABLE `jnsqta_moudle` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_notice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_notice`;

CREATE TABLE `jnsqta_notice` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `posttime` int(10) DEFAULT '0',
  `cid` int(8) DEFAULT '0',
  `adminuid` int(8) DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_notice` WRITE;
/*!40000 ALTER TABLE `jnsqta_notice` DISABLE KEYS */;

INSERT INTO `jnsqta_notice` (`id`, `posttime`, `cid`, `adminuid`, `title`, `content`)
VALUES
	(1,1571526081,1,1,'热烈庆祝金牛社区提案小程序正式上线！','');

/*!40000 ALTER TABLE `jnsqta_notice` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_proposal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_proposal`;

CREATE TABLE `jnsqta_proposal` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `posttime` int(11) DEFAULT '0',
  `cid` int(8) DEFAULT '0',
  `adminuid` int(8) DEFAULT '0',
  `serial_number` varchar(250) DEFAULT NULL,
  `reason` varchar(250) DEFAULT NULL,
  `type` int(8) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `for_community` int(11) DEFAULT NULL,
  `for_residential` varchar(250) DEFAULT NULL,
  `proposer` varchar(250) DEFAULT NULL,
  `seconder` varchar(250) DEFAULT NULL,
  `contact_phone` varchar(250) DEFAULT NULL,
  `content` text,
  `level` int(11) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `post_time` int(11) DEFAULT NULL,
  `community_audit_mind` text,
  `community_audit_time` int(11) DEFAULT NULL,
  `street_audit_mind` text,
  `street_audit_time` int(11) DEFAULT NULL,
  `district_audit_mind` text,
  `district_audit_time` int(11) DEFAULT NULL,
  `res_audit_mind` text,
  `res_audit_time` int(11) DEFAULT NULL,
  `res_should_num` int(11) DEFAULT NULL,
  `res_actual_num` int(11) DEFAULT NULL,
  `res_agree_num` int(11) DEFAULT NULL,
  `for_street` int(11) DEFAULT NULL,
  `selection` int(11) DEFAULT NULL,
  `organization` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_proposal` WRITE;
/*!40000 ALTER TABLE `jnsqta_proposal` DISABLE KEYS */;

INSERT INTO `jnsqta_proposal` (`id`, `posttime`, `cid`, `adminuid`, `serial_number`, `reason`, `type`, `category`, `for_community`, `for_residential`, `proposer`, `seconder`, `contact_phone`, `content`, `level`, `status`, `post_time`, `community_audit_mind`, `community_audit_time`, `street_audit_mind`, `street_audit_time`, `district_audit_mind`, `district_audit_time`, `res_audit_mind`, `res_audit_time`, `res_should_num`, `res_actual_num`, `res_agree_num`, `for_street`, `selection`, `organization`)
VALUES
	(1,1571571436,3,1,'','排除南堰河“小瀑布”噪声',1,1,0,'箐华园小区','徐奇伦','付明忠、曾庆京、周仲云、李文新、江忠猛','18980772730','一期小区因建成时间长，综合配套相对较弱，积存垃圾较多，上次只整治了部分，综合考量，征求居民意见在年前对修理树枝、垃圾、大件杂物等进行整治清理。\r\n建议：联系相关业务公司及志愿者对小区公共区域修理树枝、垃圾、大件杂物等进行整治清理。并安排志愿者进行长期监督、宣传。',NULL,'4',1564918364,'2019年8月8日，社区议事会共32人参加，一致同意该提案并上报街道公共议事会！',1565264522,'',0,'',0,'此案属于公共议事，情况属实，同意上报！',1565177808,29,26,26,NULL,1,NULL),
	(2,1571581004,3,1,'','两河锦地二期旁空置用地垃圾清理提案建议',2,4,0,'','徐奇伦','','18980772730','fadfsdfsdfsaf',NULL,'2',1558361703,'',0,'',0,'',0,'符合公共服务范围，同意立案！',1571587129,32,30,29,NULL,1,'惠泽路社区居委会'),
	(3,0,3,1,'','河道治理反馈',1,1,2,'88号院','吴勇','别彦华','18615711160','河道治理反馈河道治理反馈河道治理反馈河道治理反馈河道治理反馈河道治理反馈河道治理反馈河道治理反馈河道治理反馈',NULL,'2',1571584517,'2019年10月21日召开议事会，社区无法解决已经上报给街道处理！',1571587739,'',0,'',0,'同意受理，符合公共议事范围！',1571587455,32,30,29,NULL,0,'');

/*!40000 ALTER TABLE `jnsqta_proposal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_status`;

CREATE TABLE `jnsqta_status` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `posttime` int(10) DEFAULT '0',
  `cid` int(8) DEFAULT '0',
  `adminuid` int(8) DEFAULT '0',
  `status_name` varchar(250) DEFAULT NULL,
  `aorder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_status` WRITE;
/*!40000 ALTER TABLE `jnsqta_status` DISABLE KEYS */;

INSERT INTO `jnsqta_status` (`id`, `posttime`, `cid`, `adminuid`, `status_name`, `aorder`)
VALUES
	(2,1571571872,15,1,'院落受理',1),
	(3,1571571884,15,1,'已驳回',2),
	(4,1571571920,15,1,'上报社区公共议事会',3),
	(6,1571571945,15,1,'上报街道公共议事会',5),
	(8,1571571957,15,1,'上报金牛区公共议事会',7),
	(1,1571571973,15,1,'待受理',0),
	(5,1571571994,15,1,'社区受理',4),
	(7,1571572007,15,1,'街道受理',6),
	(9,1571586114,15,1,'区级部门受理',8);

/*!40000 ALTER TABLE `jnsqta_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_str
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_str`;

CREATE TABLE `jnsqta_str` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `strcid` int(5) DEFAULT NULL,
  `strname` varchar(50) DEFAULT NULL,
  `strvalue` text,
  `strinfo` varchar(250) DEFAULT NULL,
  `strstyle` varchar(250) DEFAULT NULL,
  `ifshow` tinyint(2) DEFAULT NULL,
  `ifbind` tinyint(2) DEFAULT NULL,
  `ifadmin` tinyint(2) DEFAULT NULL,
  `inputkind` tinyint(3) DEFAULT NULL,
  `strorder` int(8) DEFAULT NULL,
  `strarray` varchar(250) DEFAULT NULL,
  `ssetting` text,
  `strtip` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_str` WRITE;
/*!40000 ALTER TABLE `jnsqta_str` DISABLE KEYS */;

INSERT INTO `jnsqta_str` (`id`, `strcid`, `strname`, `strvalue`, `strinfo`, `strstyle`, `ifshow`, `ifbind`, `ifadmin`, `inputkind`, `strorder`, `strarray`, `ssetting`, `strtip`)
VALUES
	(1,0,'router','[{\"uri\":\"\\/\",\"res\":\"index\",\"domain\":\"\",\"cache\":0,\"cid\":0},{\"uri\":\"\\/1\\/\",\"res\":\"page.php\",\"cache\":0,\"cid\":\"1\",\"domain\":\"\"},{\"uri\":\"\\/1\\/page_(page).html\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"1\",\"domain\":\"\"},{\"uri\":\"\\/1\\/(id).html\",\"res\":\"article.php\",\"cache\":0,\"cid\":\"1\",\"domain\":\"\"},{\"uri\":\"\\/3\\/\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"3\",\"domain\":\"\"},{\"uri\":\"\\/3\\/page_(page).html\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"3\",\"domain\":\"\"},{\"uri\":\"\\/3\\/(id).html\",\"res\":\"article.php\",\"cache\":0,\"cid\":\"3\",\"domain\":\"\"},{\"uri\":\"\\/10\\/\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"10\",\"domain\":\"\"},{\"uri\":\"\\/10\\/page_(page).html\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"10\",\"domain\":\"\"},{\"uri\":\"\\/10\\/(id).html\",\"res\":\"article.php\",\"cache\":0,\"cid\":\"10\",\"domain\":\"\"},{\"uri\":\"\\/12\\/\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"12\",\"domain\":\"\"},{\"uri\":\"\\/12\\/page_(page).html\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"12\",\"domain\":\"\"},{\"uri\":\"\\/12\\/(id).html\",\"res\":\"article.php\",\"cache\":0,\"cid\":\"12\",\"domain\":\"\"},{\"uri\":\"\\/13\\/\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"13\",\"domain\":\"\"},{\"uri\":\"\\/13\\/page_(page).html\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"13\",\"domain\":\"\"},{\"uri\":\"\\/13\\/(id).html\",\"res\":\"article.php\",\"cache\":0,\"cid\":\"13\",\"domain\":\"\"},{\"uri\":\"\\/14\\/\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"14\",\"domain\":\"\"},{\"uri\":\"\\/14\\/page_(page).html\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"14\",\"domain\":\"\"},{\"uri\":\"\\/14\\/(id).html\",\"res\":\"article.php\",\"cache\":0,\"cid\":\"14\",\"domain\":\"\"},{\"uri\":\"\\/15\\/\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"15\",\"domain\":\"\"},{\"uri\":\"\\/15\\/page_(page).html\",\"res\":\"list.php\",\"cache\":0,\"cid\":\"15\",\"domain\":\"\"},{\"uri\":\"\\/15\\/(id).html\",\"res\":\"article.php\",\"cache\":0,\"cid\":\"15\",\"domain\":\"\"}]',NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,NULL),
	(2,0,'站点标题','金牛社区提案',NULL,NULL,NULL,NULL,0,1,5,NULL,'{\"filterhtml\":1}',NULL),
	(3,0,'关键词','金牛区社区提案 金牛区社区发展治理 社区提案',NULL,NULL,NULL,NULL,0,1,10,NULL,'{\"filterhtml\":1}',NULL),
	(4,0,'描述','金牛区社区提案是金牛区社区发展治理工作中的重大创新，为居民提供了智能便捷的基层协商通道，社区提案、智慧平台！',NULL,NULL,NULL,NULL,0,2,15,NULL,'{\"filterhtml\":1}',NULL),
	(5,0,'logo图片','',NULL,NULL,NULL,NULL,0,5,20,NULL,'{\"filterhtml\":1}',NULL),
	(6,0,'备案号','',NULL,NULL,NULL,NULL,0,1,25,NULL,'{\"filterhtml\":1}',NULL),
	(7,0,'统计代码','',NULL,NULL,NULL,NULL,0,2,30,NULL,'{\"filterhtml\":1}',NULL);

/*!40000 ALTER TABLE `jnsqta_str` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_street
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_street`;

CREATE TABLE `jnsqta_street` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `posttime` int(10) DEFAULT '0',
  `cid` int(8) DEFAULT '0',
  `adminuid` int(8) DEFAULT '0',
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_street` WRITE;
/*!40000 ALTER TABLE `jnsqta_street` DISABLE KEYS */;

INSERT INTO `jnsqta_street` (`id`, `posttime`, `cid`, `adminuid`, `name`)
VALUES
	(1,1571532441,14,1,'抚琴街道'),
	(5,1571568945,14,1,'九里堤街道'),
	(6,1571568951,14,1,'营门口街道');

/*!40000 ALTER TABLE `jnsqta_street` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jnsqta_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jnsqta_type`;

CREATE TABLE `jnsqta_type` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `posttime` int(10) DEFAULT '0',
  `cid` int(8) DEFAULT '0',
  `adminuid` int(8) DEFAULT '0',
  `type_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `jnsqta_type` WRITE;
/*!40000 ALTER TABLE `jnsqta_type` DISABLE KEYS */;

INSERT INTO `jnsqta_type` (`id`, `posttime`, `cid`, `adminuid`, `type_name`)
VALUES
	(1,1571562514,13,1,'个人提案'),
	(2,1571562528,13,1,'组织提案');

/*!40000 ALTER TABLE `jnsqta_type` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
